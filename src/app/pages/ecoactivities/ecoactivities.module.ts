import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { EcoactivitiesPage } from './ecoactivities.page';

const routes: Routes = [
  {
    path: '',
    component: EcoactivitiesPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [EcoactivitiesPage]
})
export class EcoactivitiesPageModule {}
