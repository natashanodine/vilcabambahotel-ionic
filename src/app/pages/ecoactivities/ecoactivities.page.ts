
import { Component,Inject, OnInit } from '@angular/core';

import { NavController,/* NavParams,*/ ToastController, ActionSheetController, ModalController, LoadingController }  from '@ionic/angular';
import { EcoactivitiesService } from '../../providers/ecoactivities.service';
import { FavoritesService } from '../../providers/favorites.service';
import { ActivatedRoute, Router } from '@angular/router'
import { Location } from '@angular/common';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';

import { ActionSheet, ActionSheetOptions } from '@ionic-native/action-sheet/ngx';

@Component({
  selector: 'app-ecoactivities',
  templateUrl: './ecoactivities.page.html',
  styleUrls: ['./ecoactivities.page.scss'],
})
export class EcoactivitiesPage implements OnInit {


 constructor(public api: EcoactivitiesService, 
 public loadingController: LoadingController,
	  private location: Location,
	  public route: ActivatedRoute,
	  public router: Router,
      public navCtrl: NavController, 
              /*public navParams: NavParams,*/
              public favoritesservice: FavoritesService,
              private toastCtrl: ToastController,
              private axSheetCtrl: ActionSheetController,
              private modalCtrl: ModalController,
             private socialShare: SocialSharing,
    @Inject('BaseURL') private BaseURL,	  
 ) { }
  
 ecoactivities: any;
 ecoactivity: any;

  errMess: any;
  
  favorite: boolean;
  
 
 async getEcoactivities() {
  const loading = await this.loadingController.create();
  await loading.present();
  await this.api.getEcoactivity()
    .subscribe(res => {
      console.log(res);
      this.ecoactivities = res;
      loading.dismiss();
    }, err => {
      console.log(err);
      loading.dismiss();
    });
}
 
 
 ngOnInit() {
  this.getEcoactivities();
}
  // add back when alpha.4 is out
  // navigate(item) {
  //   this.router.navigate(['/list', JSON.stringify(item)]);
  // }
  
  
  
}

