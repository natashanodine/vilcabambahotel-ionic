import { Component, OnInit } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { HousesService } from '../../providers/houses.service';

@Component({
  selector: 'app-houses',
  templateUrl: './houses.page.html',
  styleUrls: ['./houses.page.scss'],
})
export class HousesPage implements OnInit {

 constructor(public api: HousesService, public loadingController: LoadingController) { }
  
 houses: any;
 
 
 async getHouses() {
  const loading = await this.loadingController.create();
  await loading.present();
  await this.api.getHouse()
    .subscribe(res => {
      console.log(res);
      this.houses = res;
      loading.dismiss();
    }, err => {
      console.log(err);
      loading.dismiss();
    });
}
 
 
 ngOnInit() {
  this.getHouses();
}
  // add back when alpha.4 is out
  // navigate(item) {
  //   this.router.navigate(['/list', JSON.stringify(item)]);
  // }
}

