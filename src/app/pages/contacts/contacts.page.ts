import { Component } from '@angular/core';
import { LoadingController } from '@ionic/angular';

import { ActivatedRoute, Router } from '@angular/router'
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Location } from '@angular/common';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.page.html',
  styleUrls: ['./contacts.page.scss'],
})
export class ContactsPage {

	    message: any;

  contactForm: FormGroup;
  errMess: any;
  
  
  formErrors = {
    'name' : '',
    'email' : '',
    'tel' : '',
    'message' : ''
  };

  validationMessages = {
    'name' : {
      'required' : 'Name is required',
      'minlength' : 'Name must be at least 2 characters long',
      'maxlength' : 'Name cannot be more that 25 characters long'
    }
  };

  constructor(
	  public loadingController: LoadingController,
	  private fb: FormBuilder,
	  private location: Location,
	  public route: ActivatedRoute,
	  public router: Router) {
this.createForm();
	  }
	  



    createForm() {
    this.contactForm = this.fb.group({
      name: ['', [ Validators.required, Validators.minLength(2) ] ],
      email: ['', [Validators.required, Validators.email]],
      tel: ['', [ Validators.required ] ],
      message: ['', [ Validators.required ] ],
    });

    this.contactForm.valueChanges
      .subscribe(data => this.onValueChanged(data));

    this.onValueChanged(); // (re)set form validation messages
  }
  
    onValueChanged(contactFormData?: any) {
    if (!this.contactForm) {
      return;
    }
    const form = this.contactForm;
    for (const field in this.formErrors) {
      this.formErrors[field] = '';
      const control = form.get(field);
      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] += messages[key] + ' ';
        }
      }
    }

    if (this.contactForm.valid) {
      this.message = this.contactForm.value;
    } else {
      this.message = undefined;
    }
  }


  onSubmit() {
	
  

        alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.contactForm.value));
		
	 
	  
    this.contactForm.reset({
        name: '',
        email: '',
        tel: '',
        message: ''
    });

	
  }
  




}
