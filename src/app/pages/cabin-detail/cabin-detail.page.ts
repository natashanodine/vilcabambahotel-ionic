import { Component,Inject, OnInit } from '@angular/core';

import { NavController,/* NavParams,*/ ToastController, ActionSheetController, ModalController, LoadingController }  from '@ionic/angular';
import { CabinService } from '../../providers/cabin.service';
import { FavoritesService } from '../../providers/favorites.service';
import { ActivatedRoute, Router } from '@angular/router'
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Location } from '@angular/common';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';

import { ActionSheet, ActionSheetOptions } from '@ionic-native/action-sheet/ngx';

@Component({
  selector: 'app-cabin-detail',
  templateUrl: './cabin-detail.page.html',
  styleUrls: ['./cabin-detail.page.scss'],
})
export class CabinDetailPage implements OnInit {
	
	  
  cabin: any = {};
  comment: any;
  comments: Array<any>;
	
   cabins: any;
  commentForm: FormGroup;
  errMess: any;
  
  avgstars: any;
  numcomments: any;
  favorite: boolean;
  
  
  formErrors = {
    'author' : '',
    'rating' : '',
    'comment' : ''
  };

  validationMessages = {
    'author' : {
      'required' : 'Name is required',
      'minlength' : 'Name must be at least 2 characters long',
      'maxlength' : 'Name cannot be more that 25 characters long'
    }
  };

  constructor(public api: CabinService,
	  public loadingController: LoadingController,
	  private fb: FormBuilder,
	  private location: Location,
	  public route: ActivatedRoute,
	  public router: Router,
      public navCtrl: NavController, 
              /*public navParams: NavParams,*/
              public favoritesservice: FavoritesService,
              private toastCtrl: ToastController,
              private axSheetCtrl: ActionSheetController,
              private modalCtrl: ModalController,
             private socialShare: SocialSharing,
    @Inject('BaseURL') private BaseURL,	  ) {
	  this.createForm();
	  
	
	
	  }
	  
  
  async getCabin() {
  const loading = await this.loadingController.create();
  await loading.present();
  await this.api.getCabinById(this.route.snapshot.paramMap.get('id'))
    .subscribe(res => {
      console.log(res);
      this.cabin = res;
	  
	  this.favorite = this.favoritesservice.isFavorite(this.cabin.id);
    this.numcomments = this.cabin.comments.length;
    let total = 0;
    this.cabin.comments.forEach(comment => total += comment.rating );
    this.avgstars = (total/this.numcomments).toFixed(2);
      loading.dismiss();
	   
	  
	  
    }, err => {
      console.log(err);
      loading.dismiss();
    });
	
	
	
	
   
}

 ngOnInit() {
  this.getCabin();
}



    createForm() {
    this.commentForm = this.fb.group({
      author: ['', [ Validators.required, Validators.minLength(2) ] ],
      rating: 5,
      comment: ['', [ Validators.required ] ],
    });

    this.commentForm.valueChanges
      .subscribe(data => this.onValueChanged(data));

    this.onValueChanged(); // (re)set form validation messages
  }
  
    onValueChanged(commentFormData?: any) {
    if (!this.commentForm) {
      return;
    }
    const form = this.commentForm;
    for (const field in this.formErrors) {
      this.formErrors[field] = '';
      const control = form.get(field);
      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] += messages[key] + ' ';
        }
      }
    }

    if (this.commentForm.valid) {
      this.comment = this.commentForm.value;
    } else {
      this.comment = undefined;
    }
  }


  onSubmit() {
	  const id = +this.route.snapshot.paramMap.get('id');

	      this.comment['date'] = new Date().toISOString();
    this.cabin.comments.push(this.comment);
    this.api.updateComment(this.cabin.id, this.comment).subscribe(() => {
    console.log("PUT is done");
	  
    this.commentForm.reset({
        author: '',
        rating: 5,
        comment: ''
    });
})
	
  }
  
  
  
  
  
  
  
  
  
  
  async openMenu() {
    let actionSheet = await this.axSheetCtrl.create({
		
      cssClass: 'action-sheets-basic-page',
      buttons: [
       /* {
          text: 'Add to Favorties',
          handler: () => {
            this.addToFavorites();
          }
        },
        /*{
          text: 'Add a Comment',
          handler: () => {
            this.openModal();
          }
        },*/
        {
          text: 'Share via Facebook',
          handler: () => {
            this.socialShare.shareViaFacebook(this.cabin.name + ' -- ' + this.cabin.description, this.BaseURL + this.cabin.image, '')
              .then(() => console.log('Posted successfully to Facebook'))
              .catch(() => console.log('Failed to post to Facebook'));
          }
        },
        {
          text: 'Share via Twitter',
          handler: () => {
            this.socialShare.shareViaTwitter(this.cabin.name + ' -- ' + this.cabin.description, this.BaseURL + this.cabin.image, '')
              .then(() => console.log('Posted successfully to Twitter'))
              .catch(() => console.log('Failed to post to Twitter'));
          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });
    await actionSheet.present();
  }

  
  
  addToFavorites() {
    this.favorite = this.favoritesservice.addFavorite(this.cabin.id);
    this.toastCtrl.create({
      message: 'cabin ' + this.cabin.id + ' added to Favorites successfully',
      position: 'middle',
      duration: 3000
    }).then(alert => alert.present());
  }

  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  




}
