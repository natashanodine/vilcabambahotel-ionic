import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CabinDetailPage } from './cabin-detail.page';

describe('CabinDetailPage', () => {
  let component: CabinDetailPage;
  let fixture: ComponentFixture<CabinDetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CabinDetailPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CabinDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
