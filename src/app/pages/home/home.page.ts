import { Component, OnInit } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { CabinService } from '../../providers/cabin.service';
import { HousesService } from '../../providers/houses.service';
import { EcoactivitiesService } from '../../providers/ecoactivities.service';
import { ActivatedRoute, Router } from '@angular/router';



@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
 
  cabins: any;
  houses: any;
  ecoactivities: any;
  
  cabinErrMess: any;
  houseErrMess: any;
  ecoactivityErrMess: any;
  
  constructor(public cabinService: CabinService,
 public housesService: HousesService,
 public ecoactivitiesService: EcoactivitiesService,
  public loadingController: LoadingController,
  public route: ActivatedRoute,
  public router: Router) { }
  
 

 ngOnInit() {
  this.getFeaturedCabin();
  this.getFeaturedHouse();
  this.getFeaturedEcoactivity();
}

 /*
  getFeaturedCabin(): void {
    this.api.getFeaturedCabin()
    .subscribe(cabins => this.cabins = cabins);
	

    console.log(this.cabins);
  }
*/

 async getFeaturedCabin() {
  const loading = await this.loadingController.create();
  await loading.present();
  await this.cabinService.getFeaturedCabin()
    .subscribe(res => {
      console.log(res);
      this.cabins = res;
      loading.dismiss();
    }, err => {
      console.log(err);
      loading.dismiss();
    });
}



 async getFeaturedHouse() {
  const loading = await this.loadingController.create();
  await loading.present();
  await this.housesService.getFeaturedHouse()
    .subscribe(res => {
      console.log(res);
      this.houses = res;
      loading.dismiss();
    }, err => {
      console.log(err);
      loading.dismiss();
    });
}



 async getFeaturedEcoactivity() {
  const loading = await this.loadingController.create();
  await loading.present();
  await this.ecoactivitiesService.getFeaturedEcoactivity()
    .subscribe(res => {
      console.log(res);
      this.ecoactivities = res;
      loading.dismiss();
    }, err => {
      console.log(err);
      loading.dismiss();
    });
}








}
