import { Component, OnInit } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { RestApiService } from '../../providers/rest-api.service';

@Component({
  selector: 'app-list',
  templateUrl: 'list.page.html',
  styleUrls: ['list.page.scss']
})
export class ListPage implements OnInit {
 constructor(public api: RestApiService, public loadingController: LoadingController) { }
 
 cabins: any;
 
 async getCabins() {
  const loading = await this.loadingController.create();
  await loading.present();
  await this.api.getCabin()
    .subscribe(res => {
      console.log(res);
      this.cabins = res;
      loading.dismiss();
    }, err => {
      console.log(err);
      loading.dismiss();
    });
}
 
 
 ngOnInit() {
  this.getCabins();
}
  // add back when alpha.4 is out
  // navigate(item) {
  //   this.router.navigate(['/list', JSON.stringify(item)]);
  // }
}
