import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CabinsPage } from './cabins.page';

describe('CabinsPage', () => {
  let component: CabinsPage;
  let fixture: ComponentFixture<CabinsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CabinsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CabinsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
