import { Component, OnInit } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { CabinService } from '../../providers/cabin.service';

@Component({
  selector: 'app-cabins',
  templateUrl: './cabins.page.html',
  styleUrls: ['./cabins.page.scss'],
})
export class CabinsPage implements OnInit {
constructor(public api: CabinService, public loadingController: LoadingController) { }
 
 cabins: any;
 
 async getCabins() {
  const loading = await this.loadingController.create();
  await loading.present();
  await this.api.getCabin()
    .subscribe(res => {
      console.log(res);
      this.cabins = res;
      loading.dismiss();
    }, err => {
      console.log(err);
      loading.dismiss();
    });
}
 
 
 ngOnInit() {
  this.getCabins();
}
  // add back when alpha.4 is out
  // navigate(item) {
  //   this.router.navigate(['/list', JSON.stringify(item)]);
  // }
}
