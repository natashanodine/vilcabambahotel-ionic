import { Component, OnInit, Inject } from '@angular/core';

import {  NavController, /*NavParams,*/ ItemSliding, ToastController, LoadingController, AlertController } from '@ionic/angular';
import { FavoritesService } from './../../providers/favorites.service';

@Component({
  selector: 'app-favorites',
  templateUrl: './favorites.page.html',
  styleUrls: ['./favorites.page.scss'],
})
export class FavoritesPage implements OnInit {

   favorites: any = {};
   favoriteshouses: any = {};
  errMsg: string;

  constructor(public navCtrl: NavController, 
              /*public navParams: NavParams,*/
              private favservice: FavoritesService,
              private toastCtrl: ToastController,
              private loadCtrl: LoadingController,
              private alertCtrl: AlertController,
              @Inject('BaseURL') public BaseURL) {
  }

  ngOnInit() {
    this.favservice.getFavorites()
      .subscribe(favs => this.favorites = favs, errMsg => this.errMsg = errMsg);
    this.favservice.getFavoritesHouses()
      .subscribe(faves => this.favoriteshouses = faves, errMsg => this.errMsg = errMsg);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FavoritesPage');
  }

 deleteFavorite(item: ItemSliding, id: string) {
    console.log('delete', id);
    let alert = this.alertCtrl.create({
      message: 'Do you want to delete this favorite cabin?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Delete cancelled')
          }
        },
        {
          text: 'Delete',
          handler: () => {
            let loading = this.loadCtrl.create({
            });
        
            let toast = this.toastCtrl.create({
              message: 'Cabin ' + id + ' deleted successfully',
              duration: 3000
            });
        
           /* loading.present();*/
        
            this.favservice.deleteFavorite(id)
              .subscribe(favs => {
              this.favorites = favs;
              // loading.dismiss();
             //  toast.present();
            } , errMsg => {
              this.errMsg = errMsg;
             //  loading.dismiss();
            });
          }
        }
      ]
    }).then(alert => alert.present());
    item.close();
  }

}