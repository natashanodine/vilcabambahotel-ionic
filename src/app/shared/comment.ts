export class Comment {
      comment: string;
      rating: string;
      author: string;
      date?: string;
      id?:string; //sepcify the id as string if the type is string.
    }