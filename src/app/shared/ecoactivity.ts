export class Ecoactivity {
  id: string;
  name: string;
  description: string;
  image:  string;
  featured: boolean;
}