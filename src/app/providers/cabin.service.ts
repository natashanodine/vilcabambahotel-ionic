import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Http ,HttpModule} from '@angular/http';
import { catchError, tap, map, flatMap } from 'rxjs/operators';

import { baseURL } from '../shared/baseurl';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};
const apiUrl = "http://192.168.0.7:3000/cabins";


@Injectable({
  providedIn: 'root'
})
export class CabinService {

 
 constructor(private http: HttpClient,) { }
  
 private handleError(error: HttpErrorResponse) {
  if (error.error instanceof ErrorEvent) {
    // A client-side or network error occurred. Handle it accordingly.
    console.error('An error occurred:', error.error.message);
  } else {
    // The backend returned an unsuccessful response code.
    // The response body may contain clues as to what went wrong,
    console.error(
      `Backend returned code ${error.status}, ` +
      `body was: ${error.error}`);
  }
  // return an observable with a user-facing error message
  return throwError('Something bad happened; please try again later.');
}

private extractData(res: Response) {
  let body = res;
  return body || { };
}


getCabin(): Observable<any> {
  return this.http.get(apiUrl, httpOptions).pipe(
    map(this.extractData),
    catchError(this.handleError));
}


getFeaturedCabin(): Observable<any> {	
    const url = 'http://192.168.0.7:3000/cabins?featured=true';
  return this.http.get(url, httpOptions).pipe(
    map(this.extractData),
    catchError(this.handleError));
}

getCabinById(id: string): Observable<any> {
  const url = `${apiUrl}/${id}`;
  return this.http.get(url, httpOptions).pipe(
    map(this.extractData),
    catchError(this.handleError));
}

postCabin(data): Observable<any> {
  const url = `${apiUrl}/add_with_students`;
  return this.http.post(url, data, httpOptions)
    .pipe(
      catchError(this.handleError)
    );
}

updateCabin(id: string, data): Observable<any> {
  const url = `${apiUrl}/${id}`;
  return this.http.put(url, data, httpOptions)
    .pipe(
      catchError(this.handleError)
    );
}

deleteCabin(id: string): Observable<{}> {
  const url = `${apiUrl}/${id}`;
  return this.http.delete(url, httpOptions)
    .pipe(
      catchError(this.handleError)
    );
}








updateComment(id, newcomment) {
    const comment: Comment = newcomment;
    return this.http.get<any>('http://192.168.0.7:3000/cabins/' + id).pipe(
      map(cabin => {


        return {
          id: cabin.id,
          name: cabin.name,
		  image: cabin.image,
          description: cabin.description,
          priceweek: cabin.priceweek,
          pricemonth: cabin.pricemonth,
          featured: cabin.featured,
          comments: cabin.comments


        };


      }),
      flatMap((updatedCabin) => {
        updatedCabin.comments.push(comment);
        return this.http.put(apiUrl + '/' + id, updatedCabin);
      })
    );

  }



 
 
 
}
