import { TestBed } from '@angular/core/testing';

import { EcoactivitiesService } from './ecoactivities.service';

describe('EcoactivitiesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EcoactivitiesService = TestBed.get(EcoactivitiesService);
    expect(service).toBeTruthy();
  });
});
