import { Injectable } from '@angular/core';

import { Cabin } from '../shared/cabin';
import { House } from '../shared/house';
import { Http } from '@angular/http';

import { Storage } from '@ionic/storage';

import { CabinService } from './cabin.service';
import { HousesService } from './houses.service';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';


@Injectable({
  providedIn: 'root'
})
export class FavoritesService {
	
    favorites: Array<any>;

    constructor(public http: Http,
      private cabinService: CabinService,
      private housesService: HousesService,
      private storage: Storage,
      private localNotifications: LocalNotifications) {
		console.log('Hello FavoriteProvider Provider');
        storage.get('favorites').then(favorites => {
          if (favorites) {
            console.log(favorites);
            this.favorites = favorites;
          }
          else
            this.favorites = [];
          });
    }

    addFavorite(id: number): boolean {
      if (!this.isFavorite(id)) {
        this.favorites.push(id);
        this.storage.set('favorites', this.favorites);
        // Schedule a single notification
        this.localNotifications.schedule({
          id: id,
          text: 'Cabin ' + id + ' added as a favorite successfully'
        });
      }
      console.log('favorites', this.favorites);
      return true;
    }

    isFavorite(id: number): boolean {
      return this.favorites.some(el => el === id);
    }

    getFavorites(): Observable<Cabin[]> {
      return this.cabinService.getCabin()
        .map(cabins => cabins.filter(cabin => this.favorites.some(el => el === cabin.id)));
    }
	
    getFavoritesHouses(): Observable<House[]> {
      return this.housesService.getHouse()
        .map(houses => houses.filter(house => this.favorites.some(el => el === house.id)));
    }

    deleteFavorite(id: string): Observable<Cabin[]> {
      let index = this.favorites.indexOf(id);
      if (index >= 0) {
        this.favorites.splice(index,1);
        this.storage.set('favorites', this.favorites);
        return this.getFavorites();
      }
      else {
        console.log('Deleting non-existant favorite', id);
        return Observable.throw('Deleting non-existant favorite' + id);
      }
    }
}
