import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: './pages/home/home.module#HomePageModule'
  },
  {
    path: 'list',
    loadChildren: './pages/list/list.module#ListPageModule'
  },
  { path: 'cabins', loadChildren: './pages/cabins/cabins.module#CabinsPageModule' },
  { path: 'cabin-detail/:id', loadChildren: './pages/cabin-detail/cabin-detail.module#CabinDetailPageModule' },
  { path: 'houses', loadChildren: './pages/houses/houses.module#HousesPageModule' },
  { path: 'house-detail/:id', loadChildren: './pages/house-detail/house-detail.module#HouseDetailPageModule' },
  { path: 'ecoactivities', loadChildren: './pages/ecoactivities/ecoactivities.module#EcoactivitiesPageModule' },
  { path: 'contacts', loadChildren: './pages/contacts/contacts.module#ContactsPageModule' },
  { path: 'favorites', loadChildren: './pages/favorites/favorites.module#FavoritesPageModule' }
];

@NgModule({
  imports: [
	ReactiveFormsModule,
	RouterModule.forRoot(routes)
	],
  exports: [RouterModule]
})
export class AppRoutingModule {}
